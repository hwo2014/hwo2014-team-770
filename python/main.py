import json
import math
import socket
import sys

try:
    import numpy as np
    import matplotlib.pyplot as plt
except:
    pass


class NoobBot(object):
    debug = True

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_init(self, data):
        self.track = data['race']['track']
        self.pieces = self.track['pieces']
        self.cars = data['race']['cars']
        self.race_session = data['race']['raceSession']
        self.lane_dists = dict((l['index'], l['distanceFromCenter']) for l in self.track['lanes'])
        self.my_pos = None
        if self.debug:
            print data

        self.init()
        self.ping()

    def init(self):
        pass

    def on_your_car(self, data):
        self.color = data['color']
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        self.my_prev_pos = self.my_pos
        self.my_pos = next(car for car in data if car['id']['color'] == self.color)
        self.other_pos = [car for car in data if car['id']['color'] != self.color]
        if self.debug:
            print data

        self.update_state()
        self.step()

    def piece_length(self, idx):
        piece = self.pieces[idx]
        if 'length' in piece:
            return piece['length']
        else:
            dr = -self.lane_dist if piece['angle'] > 0 else self.lane_dist
            return math.pi * (piece['radius'] + dr) * abs(piece['angle']) / 180.

    def update_state(self):
        self.start_lane = self.my_pos['piecePosition']['lane']['startLaneIndex']
        self.end_lane = self.my_pos['piecePosition']['lane']['endLaneIndex']
        self.switching = (self.start_lane != self.end_lane)
        self.lane_dist = 0.5 * (self.lane_dists[self.start_lane] + self.lane_dists[self.end_lane])

        if not self.my_prev_pos:
            self.speed = 0.
        elif self.my_prev_pos['piecePosition']['pieceIndex'] == self.my_pos['piecePosition']['pieceIndex']:
            self.speed = self.my_pos['piecePosition']['inPieceDistance'] - \
                    self.my_prev_pos['piecePosition']['inPieceDistance']
        else:
            dl = self.piece_length(self.my_prev_pos['piecePosition']['pieceIndex']) - \
                    self.my_prev_pos['piecePosition']['inPieceDistance']
            self.speed = dl + self.my_pos['piecePosition']['inPieceDistance']

        self.angle_speed = self.my_pos['angle'] - (self.my_prev_pos['angle'] if self.my_prev_pos else 0.)

    def step(self):
        self.throttle(0.5)

    def on_crash(self, data):
        if data['color'] == self.color:
            print("I crashed at speed: {}".format(self.speed))
            self.on_my_crash()
        self.ping()

    def on_my_crash(self):
        pass

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_your_car,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

class BasicBot(NoobBot):
    debug = False

    def step(self):
        curr_piece = self.my_pos['piecePosition']['pieceIndex']
        Np = len(self.pieces)
        next_angle = next(i for i in range(Np) \
                if self.pieces[(curr_piece + i) % Np].get('radius'))
        if next_angle == 0:
            throttle = 0.5
        else:
            dist = self.pieces[curr_piece]['length'] - self.my_pos['piecePosition']['inPieceDistance']
            dist += sum(self.pieces[(curr_piece+i) % Np]['length'] for i in range(1, next_angle))
            # throttle = 0.5 + min(0.5, dist / 200)
            if dist < 200 or self.speed > 8.:
                throttle = 0.5
            else:
                throttle = 1.

        print throttle, self.speed
        self.throttle(throttle)

class BasicSpeedPIBot(NoobBot):
    debug = False
    store_data = False

    def init(self):
        self.alpha = 0.7
        self.delta = 2.
        self.update_p_speeds()

        if self.store_data:
            self.outfile = open('data.csv', 'w')
            self.data = []

    def update_p_speeds(self):
        Np = len(self.pieces)
        self.p_speeds = [self.alpha * math.sqrt(p['radius']) if 'radius' in p else None for p in self.pieces]
        first_angle = next(i for i in range(Np) \
                if 'radius' in self.pieces[i % Np])
        for i in range(first_angle, first_angle + Np):
            if 'radius' not in self.pieces[i%Np]:
                self.p_speeds[i%Np] = self.p_speeds[(i-1)%Np] + self.delta
        for i in range(first_angle, first_angle - Np, -1):
            if 'radius' not in self.pieces[i%Np]:
                self.p_speeds[i%Np] = min(self.p_speeds[i%Np], self.p_speeds[(i+1)%Np] + self.delta)

    def on_my_crash(self):
        self.alpha -= 0.05
        self.update_p_speeds()

    def plot_data(self):
        x = np.array(self.data)
        plt.figure()
        plt.plot(x[:,0], label='speed')
        plt.plot(x[:,1], label='target')
        plt.plot(x[:,2], label='throttle')
        plt.legend()

    def step(self):
        KP = 0.8
        curr_piece = self.my_pos['piecePosition']['pieceIndex']
        Np = len(self.pieces)
        in_dist = self.my_pos['piecePosition']['inPieceDistance']
        in_length = self.piece_length(curr_piece)

        # interpolate current speed
        target_speed = self.p_speeds[curr_piece] + in_dist / in_length * (self.p_speeds[(curr_piece+1)%Np] - self.p_speeds[curr_piece])
        throttle = KP * (target_speed - self.speed)
        print 'speed: {}, target: {}, throttle: {}'.format(self.speed, target_speed, throttle)
        if self.store_data:
            self.outfile.write('{}, {}, {}\n'.format(self.speed, target_speed, throttle))
            self.data.append([self.speed, target_speed, throttle])
        throttle = max(0., min(1., throttle))
        self.throttle(throttle)

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = BasicSpeedPIBot(s, name, key)
        bot.run()
